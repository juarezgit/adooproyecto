# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.validators import FileExtensionValidator
from django.db import models

# Create your models here.


class Administrador(models.Model):
    nombre = models.CharField(max_length=50,verbose_name='Nombre(s)')
    aPaterno = models.CharField(max_length=50, verbose_name='Apellido Paterno')
    aMaterno = models.CharField(max_length=50, blank=True, null=True, verbose_name='Apellido Materno')
    email = models.EmailField(max_length=150, verbose_name='E-mail')
    rfc = models.CharField(max_length=80, verbose_name='RFC')

    def __unicode__(self):
        return "{} {} {}".format(self.nombre, self.aPaterno,self.email)


class Analista(models.Model):
    nombre = models.CharField(max_length=50,verbose_name='Nombre(s)')
    aPaterno = models.CharField(max_length=50, verbose_name='Apellido Paterno')
    aMaterno = models.CharField(max_length=50, blank=True, null=True, verbose_name='Apellido Materno')
    email = models.EmailField(max_length=150, verbose_name='E-mail')
    rfc = models.CharField(max_length=80, verbose_name='RFC')

    def __unicode__(self):
        return "{} {} {}".format(self.nombre, self.aPaterno,self.email)


class Disenador(models.Model):
    nombre = models.CharField(max_length=50,verbose_name='Nombre(s)')
    aPaterno = models.CharField(max_length=50, verbose_name='Apellido Paterno')
    aMaterno = models.CharField(max_length=50, blank=True, null=True, verbose_name='Apellido Materno')
    email = models.EmailField(max_length=150, verbose_name='E-mail')
    rfc = models.CharField(max_length=80, verbose_name='RFC')

    def __unicode__(self):
        return "{} {} {}".format(self.nombre, self.aPaterno,self.email)


class Proyecto(models.Model):
    nombreProyecto = models.CharField(max_length=200, verbose_name='Nombre del proyecto')
    analista = models.ForeignKey('Analista')
    disenador = models.ForeignKey('Disenador')

    def __unicode__(self):
        return "{} |Analista: {}  |Disenanor: {}".format(self.nombreProyecto,self.analista, self.disenador)


class DiagramaSecuencia(models.Model):
    analista = models.ForeignKey('Analista')
    version = models.CharField(max_length=4, verbose_name='Version del diagrama')
    imagen = models.FileField(validators=[FileExtensionValidator(allowed_extensions=['jpg','png'])])


class DiagramaCasos(models.Model):
    analista = models.ForeignKey('Analista')
    version = models.CharField(max_length=4, verbose_name='Version del diagrama')
    imagen = models.FileField(validators=[FileExtensionValidator(allowed_extensions=['docx'])])


class CasosUso(models.Model):
    analista = models.ForeignKey('Analista')
    version = models.CharField(max_length=4, verbose_name='Version del diagrama')
    documento = models.FileField(validators=[FileExtensionValidator(allowed_extensions=['docx'])])


class DiagramaClases(models.Model):
    disenador = models.ForeignKey('Disenador')
    version = models.CharField(max_length=4, verbose_name='Version del diagrama')
    imagen = models.FileField(validators=[FileExtensionValidator(allowed_extensions=['docx'])])


class AdminConsultaProyecto(models.Model):
    proyecto = models.ForeignKey('Proyecto')

    def __unicode__(self):
        return "{}".format(self.proyecto)


class AnalistaConsultaDiagramaSecuencia(models.Model):
    diagramaSecuencia = models.ForeignKey('DiagramaSecuencia')

    def __unicode__(self):
        return "{}".format(self.diagramaSecuencia)


class AnalistaConsultaDiagramaCasos(models.Model):
    diagramaCasos = models.ForeignKey('DiagramaCasos')

    def __unicode__(self):
        return "{}".format(self.diagramaCasos)


class AnalistaConsultaCasosUso(models.Model):
    casosUso = models.ForeignKey('CasosUso')

    def __unicode__(self):
        return "{}".format(self.casosUso)


class DisenadorConsultaDiagramaClases(models.Model):
    diagramaClases = models.ForeignKey('DiagramaClases')

    def __unicode__(self):
        return "{}".format(self.diagramaClases)
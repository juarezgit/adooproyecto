# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render


# Create your views here.

def index(request):
    contexto={
        'title': 'Hola Django',
        'mensaje': 'Bienvenidos a Django'
    }
    return render(request, 'main/base.html', contexto)



# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from appcvs.models import *
# Register your models here.



@admin.register(Analista)
class AnalistaAdmin(admin.ModelAdmin):
    list_display = ('nombre','aPaterno','email','rfc',)


@admin.register(Disenador)
class DisenadorAdmin(admin.ModelAdmin):
    list_display = ('nombre','aPaterno','email','rfc',)


@admin.register(Proyecto)
class ProyectoAdmin(admin.ModelAdmin):
    list_display = ('nombreProyecto','analista','disenador',)


@admin.register(AdminConsultaProyecto)
class AdminConsultaProyectAodmin(admin.ModelAdmin):
    list_display = ('proyecto',)
